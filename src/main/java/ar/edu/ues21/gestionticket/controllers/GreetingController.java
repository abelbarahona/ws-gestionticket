package ar.edu.ues21.gestionticket.controllers;

import java.util.concurrent.atomic.AtomicLong;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ar.edu.ues21.gestionticket.models.Greeting;

@RestController
public class GreetingController {

    private static final String template = "Anulacion ticket id %s!";
    private final AtomicLong counter = new AtomicLong();

    @RequestMapping("/ticket")
    public Greeting greeting(@RequestParam(value="id", defaultValue="SN") String name) {
        return new Greeting(counter.incrementAndGet(),
                            String.format(template, name));
    }
}